<?php

namespace Empora\Doctrine\HelperBundle\Annotation\Cache;


/**
 * Description of RegionLifetimeInterface
 * 
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
interface RegionLifetimeInterface {

	public function getLifetime();

} 