<?php

namespace Empora\Doctrine\HelperBundle\Annotation\Cache;

use Doctrine\Common\Annotations\Annotation;


/**
 * Description of RegionLifetime
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 * @Annotation
 * @Target("CLASS")
 */
class QueryRegionLifetime extends Annotation implements RegionLifetimeInterface {

	/**
	 * @var string
	 */
	protected $prefix;

	/**
	 * @var string
	 */
	protected $suffix;

	/**
	 * @var string
	 */
	protected $affix;

	/**
	 * @return string
	 */
	public function getAffix() {
		return $this->affix;
	}

	/**
	 * @return int
	 */
	public function getLifetime() {
		return (int)$this->value;
	}

	/**
	 * @return string
	 */
	public function getPrefix() {
		return $this->prefix;
	}

	/**
	 * @return string
	 */
	public function getSuffix() {
		return $this->suffix;
	}
}