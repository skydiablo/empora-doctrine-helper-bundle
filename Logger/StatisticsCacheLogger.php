<?php

namespace Empora\Doctrine\HelperBundle\Logger;

/**
 * Description of StatisticsCacheLogger
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class StatisticsCacheLogger extends \Doctrine\ORM\Cache\Logging\StatisticsCacheLogger {
	
}
