<?php

namespace Empora\Doctrine\HelperBundle;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Logging\LoggerChain;
use Doctrine\DBAL\Logging\SQLLogger;
use Empora\Doctrine\HelperBundle\DependencyInjection\Compiler\DoctrineEntityListenerPass;
use Empora\Doctrine\HelperBundle\DependencyInjection\Compiler\DoctrineOrmConfigurationPass;
use Empora\Doctrine\HelperBundle\DependencyInjection\Compiler\RegionConfigurationPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Description of EmporaDoctrineHelperBundle
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class EmporaDoctrineHelperBundle extends Bundle {

	public function build(ContainerBuilder $container) {
		parent::build($container);
		$container->addCompilerPass(new DoctrineEntityListenerPass());
		$container->addCompilerPass(new DoctrineOrmConfigurationPass());
		$container->addCompilerPass(new RegionConfigurationPass());
	}
}
