<?php

namespace Empora\Doctrine\HelperBundle\Service;

use Doctrine\Common\Persistence\ObjectManagerAware;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\EntityManager;
use Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity;
use JMS\DiExtraBundle\Annotation AS DI;


/**
 * Description of ActiveEntityFactory
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
abstract class ActiveEntityFactory {

	/**
	 * @var EntityManager
	 * DI: doctrine.orm.entity_manager
	 */
	protected $entityManager;

	/**
	 * @param EntityManager $entityManager
	 * DI\InjectParams({
	 *     "entityManager" = DI\Inject("doctrine.orm.entity_manager")
	 * })
	 */
	function __construct(EntityManager $entityManager) {
		$this->entityManager = $entityManager;
	}

	/**
	 * @return DBEntity
	 */
	protected function createEntity() {
        $entity = call_user_func_array(array($this, 'doCreateEntity'), func_get_args());
		if ($entity instanceof ObjectManagerAware) {
			$this->assignEntityManager($entity);
		}
		return $entity;
	}

	/**
	 * @param ObjectManagerAware $entity
	 *
	 * @return ObjectManagerAware
	 */
	protected function assignEntityManager(ObjectManagerAware $entity) {
		$class = $this->entityManager->getClassMetadata(ClassUtils::getClass($entity));
		$entity->injectObjectManager($this->entityManager, $class);
		return $entity;
	}

	abstract protected function doCreateEntity($param = null);
}