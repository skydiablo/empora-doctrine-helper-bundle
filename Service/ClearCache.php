<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Empora\Doctrine\HelperBundle\Service;

use Doctrine\Common\Cache\CacheProvider;
use Doctrine\ORM\EntityManager;

/**
 * Description of ClearCache
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class ClearCache {

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    function __construct(\Psr\Log\LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function clearCache($cacheKey, EntityManager $em, $flush = false) {
        $cacheDriver = $this->getCacheImplementKey($cacheKey, $em);
        if ($cacheDriver instanceof CacheProvider) {
            /* @var $cacheDriver CacheProvider */
            $result = $cacheDriver->deleteAll();
            $message = ($result) ? 'Successfully deleted cache entries.' : 'No cache entries were deleted.';

            if ($flush) {
                $result = $cacheDriver->flushAll();
                $message = ($result) ? 'Successfully flushed cache entries.' : $message;
            }
            $this->logger->info($message);
            return true;
        } else {
            $this->logger->error(sprintf('No CacheDriver for "%s" found', $cacheKey));
            return false;
        }
    }

    protected function getCacheImplementKey($cacheKey, EntityManager $em) {
        $cacheDriver = null;
        $configuration = $em->getConfiguration();
        switch (strtolower($cacheKey)) {
            case 'metadata':
                $cacheDriver = $configuration->getMetadataCacheImpl();
                break;
            case 'query':
                $cacheDriver = $configuration->getQueryCacheImpl();
                break;
            case 'result':
                $cacheDriver = $configuration->getResultCacheImpl();
                break;
        }
        return $cacheDriver;
    }

}
