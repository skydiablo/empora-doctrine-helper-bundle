<?php

namespace Empora\Doctrine\HelperBundle\Common\Version\Exception;

/**
 * Description of VersionException
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class VersionException extends \Exception {

    const ERROR_CODE_COLUMN_LIMIT = 14709;

    static public function ColumnLimit($value, $limit) {
        return new self(sprintf('Version Column-Limit reached: Value %d; Limit %d', $value, $limit), self::ERROR_CODE_COLUMN_LIMIT);
    }
}
