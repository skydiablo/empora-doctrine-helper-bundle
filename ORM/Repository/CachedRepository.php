<?php

namespace Empora\Doctrine\HelperBundle\ORM\Repository;

use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity as DBEntityInterface;
use \Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Description of CacheReporitory
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
abstract class CachedRepository extends BaseRepository {

	private $useCache = true;
	private $isSLCActive = true;

	/**
	 * @var \Doctrine\Common\Cache\CacheProvider
	 */
	private $defaultCacheProvider;

	/**
	 * @param EntityManager $em
	 * @param ClassMetadata $class
	 */
	public function __construct(EntityManager $em, ClassMetadata $class) {
		parent::__construct($em, $class);
		$this->isSLCActive = $this->getSlcApi() instanceof \Doctrine\ORM\Cache;
	}

	/**
	 * @param \Doctrine\Common\Cache\CacheProvider $defaultCacheProvider
	 */
	public function setDefaultCacheProvider(\Doctrine\Common\Cache\CacheProvider $defaultCacheProvider) {
		$this->defaultCacheProvider = $defaultCacheProvider;
	}

	/**
	 * @return \Doctrine\Common\Cache\CacheProvider
	 */
	protected function getDefaultCacheProvider() {
		return $this->defaultCacheProvider;
	}

	/**
	 * @return bool
	 */
	public function getUseCache() {
		return (bool)$this->useCache;
	}

	/**
	 * @param bool $useCache
	 */
	public function setUseCache($useCache) {
		$this->useCache = (bool)$useCache;
	}

	/**
	 * @return \Doctrine\ORM\Cache
	 */
	protected function getSlcApi() {
		return $this->getEntityManager()->getCache();
	}

	private function evalUseCache($useCache = null) {
		if (!$this->isSLCActive) {
			$useCache = false;
		}
		if (is_bool($useCache)) {
			return $useCache;
		}
		return $this->getUseCache();
	}

	/**
	 * @param bool $useCache
	 *
	 * @return QueryBuilder
	 */
	public function getDefaultQueryBuilder($useCache = null) {
		return parent::getDefaultQueryBuilder()->setCacheable($this->evalUseCache($useCache));
	}

	/**
	 * @param string $regionName
	 *
	 * @return bool
	 */
	public function evictQueryRegion($regionName) {
		if ($this->isSLCActive) {
			return $this->getSlcApi()->getQueryCache($regionName)->clear();
		}
		return false;
	}

	/**
	 * @param DBEntityInterface $entity
	 *
	 * @return void
	 */
	public function evictEntity(DBEntityInterface $entity) {
		if ($this->isSLCActive) {
			$this->getSlcApi()->evictEntity($this->getEntityName(), $entity->getId());
		}
		/*
		 * for general usage, maybe use this style ?
		 * $this->getEntityManager()->getCache()->evictEntity(ClassUtils::getClass($entity), $entity->getId());
		 */
	}

	/**
	 * @param \Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity $entity
	 *
	 * @todo is this needed ? or is this handles bei SLC already ?
	 */
	public function remove(DBEntityInterface $entity) {
		$this->evictEntity($entity);
		parent::remove($entity);
	}
}
