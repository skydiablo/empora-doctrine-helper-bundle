<?php

namespace Empora\Doctrine\HelperBundle\ORM\Repository\Custom;

use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Description of BaseCustomRepository
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class BaseCustomRepository {

	/**
	 * @var EntityManager
	 */
	private $entityManager;

	/**
	 * @param \Doctrine\ORM\EntityManager $entityManager
	 */
	public function setEntityManager(EntityManager $entityManager) {
		$this->entityManager = $entityManager;
	}

	/**
	 * @return \Doctrine\ORM\EntityManager
	 */
	public function getEntityManager() {
		return $this->entityManager;
	}

	/**
	 * @return \Doctrine\ORM\Query\ResultSetMapping
	 */
	public function getNewResultSetMapping() {
		return new ResultSetMapping();
	}

	/**
	 *
	 * @param string $class
	 * @param string $alias
	 *
	 * @return ResultSetMapping
	 */
	public function createResultSetMapping($class, $alias) {
		$rsm = $this->getNewResultSetMapping();
		return $rsm->addEntityResult(is_object($class) ? ClassUtils::getClass($class) : ClassUtils::getRealClass($class), $alias);
	}

	/**
	 * @param       $class
	 * @param       $alias
	 * @param array $renamedColumns
	 *
	 * @return ResultSetMappingBuilder
	 */
	public function createResultSetMappingBuilder($class, $alias, $renamedColumns = array()) {
		$rsmb = new ResultSetMappingBuilder($this->getEntityManager());
		$rsmb->addRootEntityFromClassMetadata(is_object($class) ? get_class($class) : $class, $alias, $renamedColumns);
		return $rsmb;
	}

}
