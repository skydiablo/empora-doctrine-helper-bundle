<?php

namespace Empora\Doctrine\HelperBundle\ORM\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity;
use Empora\Doctrine\HelperBundle\ORM\Exception\EntityException;

class BaseRepository extends EntityRepository {

	const ENTITY = 'entity';

	private $debugMode = false;

	public function getDebugMode() {
		return (bool) $this->debugMode;
	}

	public function setDebugMode($debugMode) {
		$this->debugMode = (bool) $debugMode;
	}

	/**
	 * @return QueryBuilder
	 */
	public function getQuery() {
		return $this->getDefaultQueryBuilder()->getQuery();
	}

	/**
	 * @param string|null $indexBy
	 *
	 * @return QueryBuilder
	 */
	public function getDefaultQueryBuilder($indexBy = null) {
		return $this->createQueryBuilder(self::ENTITY, $indexBy);
	}

	/**
	 * @param \Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity $entity
	 * @throws EntityException
	 */
	public function persist(DBEntity $entity) {
		if ($this->debugMode && $entity->getId()) {
			if (!$this->getEntityManager()->contains($entity)) {
				throw EntityException::rePersist($entity);
			}
		}
		$this->getEntityManager()->persist($entity);
	}

	/**
	 * @param \Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity $entity
	 */
	public function remove(DBEntity $entity) {
		$this->getEntityManager()->remove($entity);
	}

	/**
	 * @param \Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity $entity
	 */
	public function flush(DBEntity $entity = null) {
		$this->getEntityManager()->flush($entity);
	}

	/**
	 * @param DBEntity $entity
	 * @return DBEntity
	 */
	public function save(DBEntity $entity) {
		$this->persist($entity);
		$this->flush($entity);
		return $entity;
	}

	/**
	 * @param DBEntity $entity
	 */
	public function delete(DBEntity $entity) {
		$this->remove($entity);
		$this->flush($entity);
	}

	/**
	 * @param DBEntity $entity
	 */
	public function detach(DBEntity $entity) {
		$this->getEntityManager()->detach($entity);
	}

	/**
	 * Get Entity by "id"
	 * Alias for "find"
	 * @param int $id
	 * @return DBEntity
	 * @see find
	 */
	public function getById($id) {
		return $this->find((int) $id);
	}

	/**
	 * @param int $amount
	 * @param int $offset
	 * @return DBEntity[]
	 */
	public function getAll($amount = null, $offset = null) {
		return $this->getAllQueryBuilder($amount, $offset)->getQuery()->execute();
	}

	/**
	 * @param int $amount
	 * @param int $offset
	 * @return QueryBuilder
	 */
	protected function getAllQueryBuilder($amount = null, $offset = null) {
		return $this->getDefaultQueryBuilder()->setMaxResults($amount)->setFirstResult($offset);
	}

	/**
	 * @return EntityManager
	 */
	public function getEntityManager() {
		return parent::getEntityManager();
	}

	/**
	 * Truncate Table -- disable the FOREIGN_KEY_CHECKS
	 * @return boolean
	 */
	public function truncate() {
		$classMetadata = $this->getClassMetadata();
		$connection = $this->getEntityManager()->getConnection();
		$connection->beginTransaction();
		try {
			$q = $connection->getDatabasePlatform()->getTruncateTableSql(
					$classMetadata->getTableName()
			);
			$connection->query('SET FOREIGN_KEY_CHECKS=0');
			$connection->executeUpdate($q);
			$connection->query('SET FOREIGN_KEY_CHECKS=1');
			$connection->commit();
			return true;
		} catch (\Exception $e) {
			$connection->rollback();
			return false;
		}
	}

	/**
	 * Delete ALL entitys from table -- disable the FOREIGN_KEY_CHECKS
	 * @return boolean
	 */
	public function deleteAll() {
		$classMetadata = $this->getClassMetadata();
		$connection = $this->getEntityManager()->getConnection();
		$connection->beginTransaction();
		try {
			$connection->query('SET FOREIGN_KEY_CHECKS=0');
			$connection->query('DELETE FROM ' . $classMetadata->getTableName());
			// Beware of ALTER TABLE here -- it's another DDL statement 
			// and will cause an implicit commit.
			$connection->query('SET FOREIGN_KEY_CHECKS=1');
			$connection->commit();
			return true;
		} catch (\Exception $e) {
			$connection->rollback();
			return false;
		}
	}

	/**
	 * Set the Auto-increment value
	 * @param integer $index
	 * @return boolean
	 */
	public function setAutoIncrement($index) {
		$classMetadata = $this->getClassMetadata();
		$connection = $this->getEntityManager()->getConnection();
		$connection->beginTransaction();
		try {
			$params = array(
				'index' => $index
			);
			$types = array(
				'index' => Type::INTEGER
			);
			$connection->executeQuery(
					"ALTER TABLE {$classMetadata->getTableName()} AUTO_INCREMENT = :index", $params, $types
			);
			$connection->commit();
			return true;
		} catch (\Exception $e) {
			$connection->rollback();
			return false;
		}
	}

}
