<?php

namespace Empora\Doctrine\HelperBundle\ORM\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Description of CacheUserScopeRepository
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 * @todo   change user class hint !!!
 */
abstract class UserScopeCachedRepository extends CachedRepository {

	/**
	 * @var int
	 */
	private $entityNameCRC;

	/**
	 * @var int
	 */
	private $defaultCacheRegionQueryPrefix;

	/**
	 * @param EntityManager $em
	 * @param ClassMetadata $class
	 */
	public function __construct(EntityManager $em, ClassMetadata $class) {
		parent::__construct($em, $class);
		$this->entityNameCRC = crc32($this->getEntityName()); //this will short the FQNS
		$this->defaultCacheRegionQueryPrefix = crc32(get_class($this)); //this will short the FQNS
	}

	/**
	 * default CacheRegionQueryPrefix
	 *
	 * @return int
	 */
	protected function getDefaultCacheRegionQueryPrefix() {
		return $this->defaultCacheRegionQueryPrefix;
	}

	/**
	 * @param \User  $user
	 * @param string $cachePrefix
	 * @param bool   $useCache
	 *
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	public function getDefaultQueryBuilder($useCache = null, \User $user = null, $cachePrefix = null) {
		$qb = parent::getDefaultQueryBuilder($useCache);
		if ($qb->isCacheable() && $user instanceof \User) {
			$qb->setCacheRegion($this->generateRegionQueryUserScope($user, $cachePrefix));
		}
		return $qb;
	}

	/**
	 * generate a user scope region name for current entity
	 *
	 * @param \User  $user
	 * @param string $cachePrefix
	 *
	 * @return string
	 */
	protected function generateRegionQueryUserScope(\User $user, $cachePrefix = null) {
		$cachePrefix = !is_null($cachePrefix) ? (string)$cachePrefix : $this->getDefaultCacheRegionQueryPrefix();
		return sprintf('%s-%u-%u', $cachePrefix, $this->entityNameCRC, $user->getId());
	}

	/**
	 * @param \User  $user
	 * @param string $cachePrefix
	 */
	public function evictQueryRegionByUser(\User $user, $cachePrefix = null) {
		$this->evictQueryRegion(
			$this->generateRegionQueryUserScope($user, $cachePrefix)
		);
	}
}
