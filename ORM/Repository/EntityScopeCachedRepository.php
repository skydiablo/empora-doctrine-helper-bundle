<?php

namespace Empora\Doctrine\HelperBundle\ORM\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity AS DBEntityInterface;

/**
 * Description of CacheUserScopeRepository
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
abstract class EntityScopeCachedRepository extends CachedRepository {

	/**
	 * @var int
	 */
	private $entityNameCRC;

	/**
	 * @var int
	 */
	private $defaultCacheRegionQueryPrefix;

	/**
	 * @param EntityManager $em
	 * @param ClassMetadata $class
	 */
	public function __construct(EntityManager $em, ClassMetadata $class) {
		parent::__construct($em, $class);
		$this->entityNameCRC = crc32($this->getEntityName()); //this will short the FQNS
		$this->defaultCacheRegionQueryPrefix = crc32(get_class($this)); //this will short the FQNS
	}

	/**
	 * @param bool              $useCache
	 * @param DBEntityInterface $entity
	 * @param string            $cachePrefix
	 *
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	public function getDefaultQueryBuilder($useCache = null, DBEntityInterface $entity = null, $cachePrefix = null) {
		$qb = parent::getDefaultQueryBuilder($useCache);
		if ($qb->isCacheable() && $entity instanceof DBEntityInterface) {
			$qb->setCacheRegion($this->generateRegionQueryUserScope($entity, $cachePrefix));
		}
		return $qb;
	}

	/**
	 * generate a user scope region name for current entity
	 *
	 * @param DBEntityInterface $entity
	 * @param string            $cachePrefix
	 *
	 * @return string
	 */
	protected function generateRegionQueryUserScope(DBEntityInterface $entity, $cachePrefix = null) {
		$cachePrefix = !is_null($cachePrefix) ? (string)$cachePrefix : $this->getDefaultCacheRegionQueryPrefix();
		return sprintf('%s-%u-%u', $cachePrefix, $this->entityNameCRC, $entity->getId());
	}

	/**
	 * default CacheRegionQueryPrefix
	 *
	 * @return int
	 */
	protected function getDefaultCacheRegionQueryPrefix() {
		return $this->defaultCacheRegionQueryPrefix;
	}

	/**
	 * @param DBEntityInterface $entity
	 * @param string            $cachePrefix
	 */
	public function evictQueryRegionByUser(DBEntityInterface $entity, $cachePrefix = null) {
		$this->evictQueryRegion(
			$this->generateRegionQueryUserScope($entity, $cachePrefix)
		);
	}
}
