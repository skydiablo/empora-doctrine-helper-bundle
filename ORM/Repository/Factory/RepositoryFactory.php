<?php

namespace Empora\Doctrine\HelperBundle\ORM\Repository\Factory;

use Doctrine\Common\Cache\CacheProvider;
use Doctrine\ORM\EntityManagerInterface;
use Empora\Doctrine\HelperBundle\Metadata\RegionLifetime\Factory\QueryRegionLifetimeFactory;
use Empora\Doctrine\HelperBundle\Metadata\RegionLifetime\RegionLifetimeClassMetadata;
use Empora\Doctrine\HelperBundle\Metadata\RegionLifetime\RegionLifetimeResolver;
use Empora\Doctrine\HelperBundle\ORM\Repository\BaseRepository;
use Empora\Doctrine\HelperBundle\ORM\Repository\CachedRepository;

/**
 * Description of RepositoryFactory
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class RepositoryFactory implements \Doctrine\ORM\Repository\RepositoryFactory {
    /**
     * The list of EntityRepository instances.
     *
     * @var \Doctrine\Common\Persistence\ObjectRepository[]
     */
    private $repositoryList = array();

    /**
     * @var CacheProvider
     */
    private $doctrineOrmDefaultSecondLevelCacheRegionCacheDriver;

    /**
     * @var QueryRegionLifetimeFactory
     * Empora.Doctrine.Helper.Metadata.Factory.QueryRegionLifetime
     */
    private $metadataFactoryQueryRegionLifetime;

    /**
     * @var RegionLifetimeResolver
     */
    private $regionLifetimeResolver;

    /**
     * @param CacheProvider              $doctrineOrmDefaultSecondLevelCacheRegionCacheDriver
     * @param QueryRegionLifetimeFactory $metadataFactoryQueryRegionLifetime
     * @param RegionLifetimeResolver     $regionLifetimeResolver
     */
    function __construct(CacheProvider $doctrineOrmDefaultSecondLevelCacheRegionCacheDriver, QueryRegionLifetimeFactory $metadataFactoryQueryRegionLifetime, RegionLifetimeResolver $regionLifetimeResolver) {
        $this->doctrineOrmDefaultSecondLevelCacheRegionCacheDriver = $doctrineOrmDefaultSecondLevelCacheRegionCacheDriver;
        $this->metadataFactoryQueryRegionLifetime = $metadataFactoryQueryRegionLifetime;
        $this->regionLifetimeResolver = $regionLifetimeResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository(EntityManagerInterface $entityManager, $entityName) {
        $repo = $this->_getRepository($entityManager, $entityName);

        if ($repo instanceof BaseRepository) {
            $this->collectMetadata($repo);
            $this->prepareRepository($repo);
        }
        return $repo;
    }

    /**
     * Create a new repository instance for an entity class.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager The EntityManager instance.
     * @param string                               $entityName    The name of the entity.
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function createRepository(EntityManagerInterface $entityManager, $entityName) {
        /* @var $metadata \Doctrine\ORM\Mapping\ClassMetadata */
        $metadata = $entityManager->getClassMetadata($entityName);
        $repositoryClassName = $metadata->customRepositoryClassName
            ? : $entityManager->getConfiguration()->getDefaultRepositoryClassName();

        return new $repositoryClassName($entityManager, $metadata);
    }

    protected function _getRepository(EntityManagerInterface $entityManager, $entityName) {
        $repositoryHash = $entityManager->getClassMetadata($entityName)->getName() . spl_object_hash($entityManager);

        if (isset($this->repositoryList[$repositoryHash])) {
            return $this->repositoryList[$repositoryHash];
        }

        return $this->repositoryList[$repositoryHash] = $this->createRepository($entityManager, $entityName);
    }

    protected function collectMetadata(BaseRepository $repo) {
        foreach ($this->metadataFactoryQueryRegionLifetime->getMetadataForClass(get_class($repo))->classMetadata AS $metadata) {
            if ($metadata instanceof RegionLifetimeClassMetadata) {
                /* @var $metadata RegionLifetimeClassMetadata */
                $this->regionLifetimeResolver->addRegionLifetimes($metadata->getRegionLifetimeList());
            }
        }
    }

    protected function prepareRepository(BaseRepository $repo) {
        if ($repo instanceof CachedRepository) {
            $repo->setDefaultCacheProvider($this->doctrineOrmDefaultSecondLevelCacheRegionCacheDriver);
        }
    }
}