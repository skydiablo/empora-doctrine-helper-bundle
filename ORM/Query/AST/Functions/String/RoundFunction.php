<?php

namespace Empora\Doctrine\HelperBundle\ORM\Query\AST\Functions\String;

use \Doctrine\ORM\Query\AST\Functions\FunctionNode;
use \Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Description of Round
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class RoundFunction extends FunctionNode {

	public $firstDateExpression = null;
	public $secondDateExpression = null;

	public function parse(Parser $parser) {
		$parser->match(Lexer::T_IDENTIFIER);
		$parser->match(Lexer::T_OPEN_PARENTHESIS);
		$this->firstDateExpression = $parser->ArithmeticPrimary();
		$parser->match(Lexer::T_COMMA);
		$this->secondDateExpression = $parser->ArithmeticPrimary();
		$parser->match(Lexer::T_CLOSE_PARENTHESIS);
	}

	public function getSql(SqlWalker $sqlWalker) {
		return sprintf(
			'ROUND(%s, %s)',
			$this->firstDateExpression->dispatch($sqlWalker),
			$this->secondDateExpression->dispatch($sqlWalker)
		);
	}
}