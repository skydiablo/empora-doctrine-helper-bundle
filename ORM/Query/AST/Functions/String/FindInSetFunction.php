<?php

namespace Empora\Doctrine\HelperBundle\ORM\Query\AST\Functions\String;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Description of FindInSetFunction
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class FindInSetFunction extends FunctionNode {

    public $needle = null;
    public $haystack = null;

    public function parse(Parser $parser) {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->needle = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->haystack = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(SqlWalker $sqlWalker) {
        return sprintf(
            'FIND_IN_SET(%s, %s)',
            $this->needle->dispatch($sqlWalker),
            $this->haystack->dispatch($sqlWalker)
        );
    }
}