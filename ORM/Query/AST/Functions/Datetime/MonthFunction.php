<?php

namespace Empora\Doctrine\HelperBundle\ORM\Query\AST\Functions\Datetime;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;


/**
 * Description of MonthFunction
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class MonthFunction extends FunctionNode {

	protected $date;

	public function parse(Parser $parser) {
		$parser->match(Lexer::T_IDENTIFIER);
		$parser->match(Lexer::T_OPEN_PARENTHESIS);
		$this->date = $parser->ArithmeticPrimary();
		$parser->match(Lexer::T_CLOSE_PARENTHESIS);
	}

	public function getSql(SqlWalker $sqlWalker) {
		return sprintf(
			'MONTH(%s)',
			$sqlWalker->walkArithmeticPrimary($this->date)
		);
	}
}