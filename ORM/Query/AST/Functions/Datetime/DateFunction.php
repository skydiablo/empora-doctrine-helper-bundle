<?php

namespace Empora\Doctrine\HelperBundle\ORM\Query\AST\Functions\Datetime;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Description of DateFunction
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class DateFunction extends FunctionNode {

    protected $dateExpression;

    public function parse(Parser $parser) {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->dateExpression = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(SqlWalker $sqlWalker) {
        return sprintf(
            'DATE(%s)',
            $this->dateExpression->dispatch($sqlWalker)
        );
    }
} 