<?php

namespace Empora\Doctrine\HelperBundle\ORM\Exception;

use Doctrine\Common\Util\ClassUtils;
use Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity;

/**
 * Description of EntityException
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class EntityException extends \Exception {

	const CODE_OBJECT_MANAGER_MISSING = 100;
	const CODE_REPERSIST = 101;

	public static function objectManagerMissing() {
		return new self('ObjectManager missing', self::CODE_OBJECT_MANAGER_MISSING);
	}

	public static function rePersist(DBEntity $entity) {
		return new self(sprintf('Trying to re-persist entity: %s [ID: %u]', ClassUtils::getClass($entity), $entity->getId()), self::CODE_REPERSIST);
	}

}
