<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Empora\Doctrine\HelperBundle\ORM\Entity;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectManagerAware;
use Empora\Doctrine\HelperBundle\ORM\Exception\EntityException;

/**
 * Description of ActiveDBEntity
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
abstract class ActiveDBEntity extends DBEntity implements ObjectManagerAware {

	/**
	 * @var ObjectManager
	 */
	protected $_objectManager;

	/**
	 * @var ClassMetadata
	 */
	protected $_classMetaData;

	public function injectObjectManager(ObjectManager $objectManager, ClassMetadata $classMetadata) {
		$this->_objectManager = $objectManager;
		$this->_classMetaData = $classMetadata;
	}

	/**
	 * @return ObjectManager
	 * @throws EntityException
	 */
	protected function getObjectManager() {
		if ($this->_objectManager instanceof ObjectManager) {
			return $this->_objectManager;
		} else {
			throw EntityException::objectManagerMissing();
		}
	}

	/**
	 * @see persist
	 * @see flush
	 */
	public function save() {
		$this->persist();
		$this->flush();
	}

	/**
	 * @see remove
	 * @see flush
	 */
	public function delete() {
		$this->remove();
		$this->flush();
	}

	/**
	 * Flushes all changes to objects that have been queued up to now to the database.
	 * This effectively synchronizes the in-memory state of managed objects with the
	 * database.
	 */
	public function flush() {
		$this->getObjectManager()->flush();
	}

	/**
	 * Tells the ObjectManager to make an instance managed and persistent.
	 *
	 * The object will be entered into the database as a result of the flush operation.
	 *
	 * NOTE: The persist operation always considers objects that are not yet known to
	 * this ObjectManager as NEW. Do not pass detached objects to the persist operation.
	 */
	public function persist() {
		$this->getObjectManager()->persist($this);
	}

	/**
	 * Removes an object instance.
	 *
	 * A removed object will be removed from the database as a result of the flush operation.
	 */
	public function remove() {
		$this->getObjectManager()->remove($this);
	}

	/**
	 * Refreshes the persistent state of an object from the database,
	 * overriding any local changes that have not yet been persisted.
	 */
	public function refresh() {
		$this->getObjectManager()->refresh($this);
	}

	/**
	 * Merges the state of a detached object into the persistence context
	 * of this ObjectManager and returns the managed copy of the object.
	 * The object passed to merge will not become associated/managed with this ObjectManager.
	 */
	public function merge() {
		$this->getObjectManager()->merge($this);
	}

	/**
	 * Detaches an object from the ObjectManager, causing a managed object to
	 * become detached. Unflushed changes made to the object if any
	 * (including removal of the object), will not be synchronized to the database.
	 * Objects which previously referenced the detached object will continue to
	 * reference it.
	 */
	public function detach() {
		$this->getObjectManager()->detach($this);
	}

}
