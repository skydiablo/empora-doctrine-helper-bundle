<?php

namespace Empora\Doctrine\HelperBundle\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 * 
 * @ORM\MappedSuperclass
 */
abstract class DBEntity implements Interfaces\DBEntity {

	/**
	 * @var int
	 * 
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id; //todo: set to private ????

	/**
	 * @return int
	 */
	public function getId() {
		return (int) $this->id;
	}

}

?>
