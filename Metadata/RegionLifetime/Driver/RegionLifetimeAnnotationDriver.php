<?php

namespace Empora\Doctrine\HelperBundle\Metadata\RegionLifetime\Driver;

use Doctrine\Common\Annotations\Reader;
use Empora\Doctrine\HelperBundle\Annotation\Cache\RegionLifetimeInterface;
use Empora\Doctrine\HelperBundle\Metadata\RegionLifetime\RegionLifetimeClassMetadata;
use Metadata\Driver\DriverInterface;


/**
 * Description of AnnotationDriver
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class RegionLifetimeAnnotationDriver implements DriverInterface {

	/**
	 * @var \Doctrine\Common\Annotations\Reader
	 */
	private $reader;

	/**
	 * @param Reader $reader
	 */
	public function __construct(Reader $reader) {
		$this->reader = $reader;
	}

	/**
	 * @param \ReflectionClass $class
	 *
	 * @return \Metadata\ClassMetadata
	 */
	public function loadMetadataForClass(\ReflectionClass $class) {
		$classMetadata = new RegionLifetimeClassMetadata();
		foreach ($this->reader->getClassAnnotations($class) AS $annotation) {
			if ($annotation instanceof RegionLifetimeInterface) {
				$classMetadata->addRegionLifetime($annotation);
			}
		}
		return $classMetadata;
	}
}