<?php

namespace Empora\Doctrine\HelperBundle\Metadata\RegionLifetime\Factory;

use Metadata\Driver\DriverInterface;
use Metadata\MetadataFactory;

/**
 * Description of QueryRegionLifetimeFactory
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class QueryRegionLifetimeFactory extends MetadataFactory {

	/**
	 * @param DriverInterface $driver
	 */
	public function __construct(DriverInterface $driver) {
		parent::__construct($driver);
	}
}