<?php

namespace Empora\Doctrine\HelperBundle\Metadata\RegionLifetime;

use Doctrine\Common\Collections\ArrayCollection;
use Empora\Doctrine\HelperBundle\Annotation\Cache\RegionLifetimeInterface;
use Metadata\ClassMetadata as BaseClassMetadata;

/**
 * Description of PropertyMetadata
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class RegionLifetimeClassMetadata extends BaseClassMetadata {

	/**
	 * @var ArrayCollection of RegionLifetime
	 */
	protected $regionLifetimeList;

	function __construct() {
		$this->regionLifetimeList = new ArrayCollection();
	}

	/**
	 * @param RegionLifetimeInterface $regionLifetime
	 */
	public function addRegionLifetime(RegionLifetimeInterface $regionLifetime) {
		$this->regionLifetimeList->add($regionLifetime);
	}

	/**
	 * @return ArrayCollection of \Empora\Doctrine\HelperBundle\Annotation\Cache\RegionLifetime
	 */
	public function getRegionLifetimeList() {
		return $this->regionLifetimeList;
	}

}