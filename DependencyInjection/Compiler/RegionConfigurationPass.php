<?php

namespace Empora\Doctrine\HelperBundle\DependencyInjection\Compiler;

use Empora\Doctrine\HelperBundle\ORM\Cache\RegionsConfiguration;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;


/**
 * Description of RegionConfigurationPass
 * 
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class RegionConfigurationPass implements CompilerPassInterface {

	/**
	 * You can modify the container here before it is dumped to PHP code.
	 *
	 * @param ContainerBuilder $container
	 *
	 * @api
	 */
	public function process(ContainerBuilder $container) {
		$entityManagerName = 'default'; //$entityManager['name']
		$regionConfigDefinitionName = sprintf('doctrine.orm.%s_second_level_cache.regions_configuration', $entityManagerName);
		if($container->has($regionConfigDefinitionName)) {
			$container->setParameter('doctrine.orm.second_level_cache.regions_configuration.class', RegionsConfiguration::class);
			$regionConfDef = $container->getDefinition($regionConfigDefinitionName);
			$regionConfDef->addMethodCall('setRegionLifetimeResolver', array(new Reference('Empora.Doctrine.Helper.Metadata.RegionLifetime.Resolver')));
		}
	}
}