<?php

namespace Empora\Doctrine\HelperBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;


/**
 * Description of DoctrineOrmConfigurationPass
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class DoctrineOrmConfigurationPass implements CompilerPassInterface {


	/**
	 * You can modify the container here before it is dumped to PHP code.
	 *
	 * @param ContainerBuilder $container
	 *
	 * @api
	 */
	public function process(ContainerBuilder $container) {
		$factory = $container->findDefinition('Empora.Doctrine.ORM.Repository.Factory.RepositoryFactory');
		$container->findDefinition('doctrine.orm.configuration')->addMethodCall('setRepositoryFactory', array($factory));
	}
}