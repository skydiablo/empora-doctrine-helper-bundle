<?php

namespace Empora\Doctrine\HelperBundle\DependencyInjection\Compiler;

use Empora\Doctrine\HelperBundle\ORM\Mapping\EntityListenerResolver;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Description of DoctrineEntityListenerPass
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class DoctrineEntityListenerPass implements CompilerPassInterface {

	public function process(ContainerBuilder $container) {
		$definition = $container->getDefinition(EntityListenerResolver::SERVICE_NAME);
		$services = $container->findTaggedServiceIds('doctrine.entity_listener');

		foreach (array_keys($services) as $service) {
			$definition->addMethodCall(
					'addMapping', array($container->getDefinition($service)->getClass(), $service)
			);
		}
	}

}
