<?php

namespace Empora\Doctrine\HelperBundle\DependencyInjection;

use Empora\Doctrine\HelperBundle\ORM\Mapping\EntityListenerResolver;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class EmporaDoctrineHelperExtension extends Extension implements PrependExtensionInterface {

	protected $dqlFunctionsBaseDir;
	protected $dbalTypesBaseDir;

	function __construct() {
		$bundleBaseDir = realpath(__DIR__ . '/../');
		$this->dqlFunctionsBaseDir = realpath(sprintf('%s/%s', $bundleBaseDir, 'ORM/Query/AST/Functions'));
		$this->dbalTypesBaseDir = realpath(sprintf('%s/%s', $bundleBaseDir, 'DBAL/Type'));
	}


	/**
	 * {@inheritDoc}
	 */
	public function load(array $configs, ContainerBuilder $container) {
		$configuration = new Configuration();
		$config = $this->processConfiguration($configuration, $configs);

		$loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
		$loader->load('services.yml');
	}

	/**
	 * Allow an extension to prepend the extension configurations.
	 *
	 * @param ContainerBuilder $container
	 *
	 */
	public function prepend(ContainerBuilder $container) {
		$dqlTypes = array();
		foreach ($this->collectDQLFunctionTypes() AS $type) {
			$dqlTypes[strtolower(sprintf('%s_functions', $type))] = $this->collectDQLFunctions($type);
		}

		$container->prependExtensionConfig('doctrine', array(
			'dbal' => array(
				'types' => $this->collectDBALTypes()
			),
			'orm' => array(
				'entity_managers' => array(
					'default' => array(
						'entity_listener_resolver' => EntityListenerResolver::SERVICE_NAME,
						'dql' => $dqlTypes
					)
				)
			)
		));
	}

	protected function collectDQLFunctionTypes() {
		$result = array();
		$finder = new Finder();
		foreach ($finder->directories()->in($this->dqlFunctionsBaseDir)->depth(0) AS $file) {
			/* @var $file SplFileInfo */
			$result[] = $file->getBasename();
		}
		return $result;
	}

	/**
	 * @param string $type
	 *
	 * @return array
	 */
	protected function collectDQLFunctions($type) {
		static $namespacePrefix = 'Empora\Doctrine\HelperBundle\ORM\Query\AST\Functions';
		$functionType = ucfirst(strtolower($type));
		$result = array();
		$finder = new Finder();
		foreach ($finder->files()->in(sprintf('%s/%s', $this->dqlFunctionsBaseDir, $functionType))->name('*Function.php')->depth(0) AS $file) {
			/* @var $file SplFileInfo */
			$result[strtoupper($this->uncamelize($file->getBasename('Function.php')))] = sprintf('%s\%s\%s', $namespacePrefix, $functionType, $file->getBasename('.php'));
		}
		return $result;
	}

	protected function collectDBALTypes() {
		static $namespacePrefix = 'Empora\Doctrine\HelperBundle\DBAL\Type';
		$result = array();
		$finder = new Finder();
		foreach ($finder->files()->in($this->dbalTypesBaseDir)->name('*Type.php')->depth(0) AS $file) {
			/* @var $file SplFileInfo */
			$result[lcfirst($file->getBasename('Type.php'))] = sprintf('%s\%s', $namespacePrefix, $file->getBasename('.php'));
		}
		return $result;
	}

	protected function uncamelize($camelCase, $delimiter = '_') {
		return strtolower(preg_replace('/([A-Z])/', $delimiter . '$1', lcfirst($camelCase)));
	}
}
