<?php

namespace Empora\Doctrine\HelperBundle\Listener\JMS\Serializer;
use Doctrine\Common\Persistence\ObjectManagerAware;
use Empora\Doctrine\HelperBundle\Service\ActiveEntityFactory;
use JMS\Serializer\EventDispatcher\ObjectEvent;


/**
 * Description of AssignEntityManager
 * 
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class AssignEntityManager extends ActiveEntityFactory {

	protected function doCreateEntity($param = null) {
		//not needed...
	}

	public function postDeserialize(ObjectEvent $event) {
		if($event->getObject() instanceof ObjectManagerAware) {
			$this->assignEntityManager($event->getObject());
		}
	}
}