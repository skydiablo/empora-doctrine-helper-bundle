<?php

namespace Empora\Doctrine\HelperBundle\DBAL\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Type needs Hex input!
 */
class BitstreamType extends Type {

	const __TYPE_NAME = 'bitstream';

	public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
		return "BINARY COMMENT '(DC2Type:" . static::__TYPE_NAME . ")'";
	}

	public function convertToPHPValue($value, AbstractPlatform $platform) {
		$value = $this->bin2Hex($value);
		return $value;
	}

	public function convertToDatabaseValue($value, AbstractPlatform $platform) {
		$value = $this->hex2Bin($value);
		return $value;
	}

	public function getName() {
		return static::__TYPE_NAME;
	}

	protected function hex2Bin($data) {
		return pack('H*', $data);
	}
	
	protected function bin2Hex($data) {
		return bin2hex($data);
	}

}
