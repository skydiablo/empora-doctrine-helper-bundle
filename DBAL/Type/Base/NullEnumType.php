<?php

namespace Empora\Doctrine\HelperBundle\DBAL\Type\Base;

use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Description of NullEnumType
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
abstract class NullEnumType extends EnumType {

	public function convertToDatabaseValue($value, AbstractPlatform $platform) {
		if (!is_null($value)) {
			return parent::convertToDatabaseValue($value, $platform);
		}
		return $value;
	}

}
