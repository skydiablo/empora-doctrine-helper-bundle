<?php

namespace Empora\Doctrine\HelperBundle\DBAL\Type\Base;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Description of EnumType
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
abstract class EnumType extends Type {

    const __TYPE_NAME = '__UNDEFINED__';

    const PLATFORM_MYSQL = 'MYSQL';

    /**
     * @var array
     */
    protected static $values = [];

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
        $values = array_map(function ($val) use ($platform) {
            return $platform->quoteIdentifier($val);
        }, static::getValues());

        switch ($name = strtoupper($platform->getName())) {
            case self::PLATFORM_MYSQL:
                return sprintf('ENUM(%s) COMMENT "(DC2Type:%s)"', implode(",", $values), (string)$this);
                break;
            default:
                $fieldDeclaration['length'] = max(array_map('strlen', $values)) - (strlen($platform->getIdentifierQuoteCharacter()) * 2); // subtract the quotes
                $result = $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
                return $result . ' ' . sprintf('CHECK(%s IN(%s))', $fieldDeclaration['name'], implode(',', $values));
                break;
        }
    }

    /**
     * @param                  $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToPHPValue($value, AbstractPlatform $platform) {
        return $value;
    }

    /**
     * @param                  $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        if (!in_array($value, static::getValues())) {
            throw new \InvalidArgumentException(sprintf('The input value "%s" is invalid for enum "%s"', $value, (string)$this));
        }
        return $value;
    }

    /**
     * @return string
     */
    function __toString() {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getName() {
        return static::__TYPE_NAME;
    }

    /**
     * @return array
     */
    public static function getValues() {
        return static::$values;
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function boot() {
        if(!static::hasType(static::__TYPE_NAME)) {
            static::addType(static::__TYPE_NAME, static::class);
        }
    }
}
