<?php

namespace Empora\Doctrine\HelperBundle\DBAL\Type;

/**
 * Description of GenderType
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class GenderType extends Base\NullEnumType {

	const __TYPE_NAME = 'gender';
	const MALE = 'male';
	const FEMALE = 'female';
	const UNISEX = 'unisex';

	protected static $values = [self::MALE, self::FEMALE, self::UNISEX];

}
